This is a Docker image with Unity, in order to run CI builds.

I'm thinking of writing a tutorial on getting a Unity CI build set up. In the meantime, here are some links I've found helpful:

- [Unity command line arguments](https://docs.unity3d.com/Manual/CommandLineArguments.html)
- [Unity BuildPipeline.BuildPlayer documentation](https://docs.unity3d.com/ScriptReference/BuildPipeline.BuildPlayer.html)
- [Run Unity Personal inside a Docker container](http://www.robinryf.com/blog/2017/09/30/running-unity-inside-docker.html)